﻿salam
public void addDanger(String key, Danger danger)
{
	dangers.put(key, danger);
	Runtime rt = Runtime.instance();
	try {
		String host = this.profile.getSpecifiers(
			Profile.MAIN_HOST).get(0).toString();
		String port = this.profile.getSpecifiers(
			Profile.MAIN_PORT).get(0).toString();
		Profile profile = new ProfileImpl(
			host, Integer.valueOf(port), key);
		profile.setParameter(Profile.MAIN, false);
	        AgentContainer ac = rt.
			createAgentContainer(profile);
		ContainerID cId = new ContainerID(key, null);
		cId.setAddress(localhost);
		cId.setMain(false);
		cId.setPort("8888");
		danger.setContainerID(cId);
		danger.setAgentContainer(ac);
		} catch (Exception e) {
		e.printStackTrace();
		}
	}
	public Danger getDanger(String key) {
		return dangers.get(key);
	}
	public void removeAllDangers() {
		dangers.clear();
	}
	public Hashtable<String, Danger> getAllDangers() {
		return dangers; 
	}
	public void removeDanger(String key) {
		Danger dng = dangers.get(key);
		// Delete the container on JADE and JAMDER
		try {
			dng.getContainerController().kill();
		} catch (StaleProxyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		dangers.remove(key);
	} 