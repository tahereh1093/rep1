DisasterClass Disaster_Class_Name
	Disaster_Type Concept_Name
	Hazard setOf {Hazard_Name}
	Vulnerability setOf {Vulnerability_Name}
	Risk setOf {Risk_Name}
	Properties setOf {Property_Name}
	Relationships setOf {Relationship_Name}
End Disaster_Class_Name
