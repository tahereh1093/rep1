package ERE_Casestudy_Fokoshima;


import jamder.behavioural.Task;

import jamder.structural.capability;

import jamder.structural.Resource;

import jamder.structural.Command;

import jamder.Environment;


public class DetectionAndAnalysisOfRadioactiveContamination
	 extends Task 
{

//constructor
	
public DetectionAndAnalysisOfRadioactiveContamination
	 (String name, Environment env){

  super(name, env);


  // capabilities, Resource and Commands
	
  capability ManagementOfNuclearAccidents= 
	new capability("ManagementOfNuclearAccidents",
		"String",null);
	
  addcapability("ManagementOfNuclearAccidents",
		ManagementOfNuclearAccidents);

	
  Resource res1= new Resource("res1","String",null);
	
  addResource("res1",res1);



// Teams execute this task
	
  addteam("ConsequencesOfAmericaManagementTeam",
	env.getOrganization("NuclearPowerPlantOrganization").
	getTeam("ConsequencesOfAmericaManagementTeam"));

}
}
