sala
Agent_Class Agent_Class_Name
	Beliefs setOf {Belief_Name}
	Actions setOf {Action_Name}
	Plans setOf {Plan_Name}
	Events generated: setOf {Event_Name,
	Perceived: setOf{Event_Name}}
	Roles setOf {Role_Class_Name}
	Capability setOf {Capability_Name}
	Relationships setOf {Relationship_Name}
End Agent_Class
